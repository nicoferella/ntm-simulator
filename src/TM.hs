{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -O2 #-}

module TM where
import Data.List
import Data.List.Split
import Data.Maybe
import GHC.Conc
-- parMap, rpar
import Control.Parallel.Strategies
-- expresiones regulares
import Text.Regex.Posix

emptyChar = '\223' -- El 'ß' es el vacio

data Move = L | R | S

data Triplet = Triplet { setState :: String,
                         setChar :: (Maybe Char),
                         movement :: Move } deriving(Show)

data Transition = Transition { withState :: String,
                               withChar :: (Maybe Char),
                               triplets :: [Triplet] } deriving(Show)

data TuringMachine = TuringMachine { qs :: [String], 
                                     sigma :: [Char], 
                                     gamma :: [Char], 
                                     delta :: [Transition],
                                     q0 :: String,
                                     qa :: String,
                                     qr :: String } deriving(Show)

data Snap = Snap { idSnap :: String, 
                   parent :: String, 
                   depth :: Int, 
                   tape :: [Maybe Char], 
                   actualState :: String, 
                   tapeLocation :: Int } deriving(Show,Eq)

instance Show Move where
    show (L) = "L"
    show (R) = "R"
    show (S) = "S"

checkStateWithChar :: String -> Maybe Char -> Transition -> Bool
checkStateWithChar state char transition = (withState transition == state) && (withChar transition == char)

getTransitionWithStateWithChar :: [Transition] -> String -> Maybe Char -> Maybe Transition
getTransitionWithStateWithChar xs state char = find (\x -> checkStateWithChar state char x) xs

moveTape :: Move -> Int -> [Maybe Char] -> [Maybe Char]
moveTape L 0 x = Nothing:x
moveTape L tapeLocation x = x
moveTape R tapeLocation x = 
    if (tapeLocation == length(x)-1) then x++[Nothing] else x

setCharInTapePos :: [Maybe Char] -> Maybe Char -> Int -> [Maybe Char]
setCharInTapePos [] c _ = [c]
setCharInTapePos (x:xs) c 0 = c:xs
setCharInTapePos (x:xs) c n = x:(setCharInTapePos xs c (n-1))

moveTapeSnap :: (Int, Snap) -> Int -> Maybe Char -> String -> Move -> Snap
moveTapeSnap (posS, snap) posT setChar setState L = Snap { idSnap = "d"++show(depth snap + 1)++"s"++show posS++"t"++show posT, parent = idSnap snap, depth = (depth snap + 1), tape = moveTape L (tapeLocation snap) (setCharInTapePos (tape snap) setChar (tapeLocation snap)), actualState = setState, tapeLocation = max 0 ((tapeLocation snap)-1) }
moveTapeSnap (posS, snap) posT setChar setState R = Snap { idSnap = "d"++show(depth snap + 1)++"s"++show posS++"t"++show posT, parent = idSnap snap, depth = (depth snap + 1), tape = moveTape R (tapeLocation snap) (setCharInTapePos (tape snap) setChar (tapeLocation snap)), actualState = setState , tapeLocation = (tapeLocation snap) + 1 }
moveTapeSnap (posS, snap) posT setChar setState S = Snap { idSnap = "d"++show(depth snap + 1)++"s"++show posS++"t"++show posT, parent = idSnap snap, depth = (depth snap + 1), tape = (setCharInTapePos (tape snap) setChar (tapeLocation snap)), actualState = setState , tapeLocation = tapeLocation snap }

applyTripletToSnap :: (Int, Triplet) -> (Int, Snap) -> Snap
applyTripletToSnap (posT,t) s = moveTapeSnap s posT (setChar t) (setState t) (movement t)

applyTransitionToSnap :: Maybe Transition -> (Int, Snap) -> [Snap]
applyTransitionToSnap Nothing s = []
applyTransitionToSnap (Just t) s = parMap rpar (\(posT,t) -> applyTripletToSnap (posT,t) s) (tripletsWithPos) 
                                        where tripletsWithPos = zip [0..] (triplets t) :: [(Int, Triplet)]

getTapeContentAtLocation :: [Maybe Char] -> Int -> Maybe Char
getTapeContentAtLocation [] _ = Nothing
getTapeContentAtLocation tape pos = tape!!pos

executeSnapsOneStep :: [Snap] -> [Transition] -> [Snap]
executeSnapsOneStep ss ts = concat (parMap rpar 
                                (\(pos,x) -> applyTransitionToSnap 
                                    (getTransitionWithStateWithChar ts (actualState x) (getTapeContentAtLocation (tape x) (tapeLocation x))) (pos, x)) (snapsWitPos)) 
                                        where snapsWitPos = zip [0..] (ss) :: [(Int, Snap)]

getMove :: Char -> Move
getMove 'L' = L
getMove 'R' = R
getMove 'S' = S

tripletParser :: String -> Triplet
tripletParser s = let (_, _, _, [stateToBeSet, charToBeSet, moveToBeSet]) = s =~ "\\((.+),(.),(.)\\)" :: (String,String,String,[String])
    in Triplet { setState = stateToBeSet, setChar = (if ((head charToBeSet) == emptyChar) then Nothing else Just (head charToBeSet)), movement = (getMove (head moveToBeSet)) }

tripletsParser :: String -> [Triplet] 
tripletsParser tripletsToConsume = do
    let listOfTriplets = splitOn ";" (filter (\x -> not (elem x ['{','}',' '])) (tripletsToConsume))
    map (tripletParser) listOfTriplets

transitionParser :: String -> Transition
transitionParser transitionToConsume = do
    let splitTransition = splitOn "=" (filter (\x -> not (elem x ['{','}',' '])) (transitionToConsume))
    let (_, _, _, [stateToRead, charToRead]) = (head splitTransition) =~ "\\((.+),(.)\\)" :: (String,String,String,[String])
    Transition { withState = stateToRead, withChar = (if ((head charToRead) == emptyChar) then Nothing else Just (head charToRead)), triplets = (tripletsParser(head (tail splitTransition)))}

deltaParser :: String -> Maybe [Transition]
deltaParser deltaToConsume = if (deltaVerify deltaToConsume) then Just (map transitionParser (filter (not . null) (lines deltaToConsume))) else Nothing

transitionVerify :: String -> Bool
transitionVerify s = do
    let (left, match, right) = (filter (\x -> not (elem x [' '])) s) =~ "\\([^,;+=]+,[^,;+=]\\)=(\\([^,;+=]+,[^,;+=],(R|L|S)\\)|\\{\\([^,;+=]+,[^,;+=],(R|L|S)\\)(;\\([^,;+=]+,[^,;+=],(R|L|S)\\))*\\})" :: (String,String,String)
    (left == "" && right == "")

deltaVerify :: String -> Bool
deltaVerify s = all (transitionVerify) (lines (filter (\x -> not (elem x [' '])) s))