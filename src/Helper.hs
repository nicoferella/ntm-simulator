module Helper where

import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny.Core
import Data.List.Split
import Data.List (intercalate,group,sort)
import TM

parseStringToTape :: String -> [Maybe Char]
parseStringToTape tape = map (\x -> getMaybeCharFromChar x) tape

getMaybeCharFromChar :: Char -> Maybe Char
getMaybeCharFromChar char = if (char == emptyChar) then Nothing else Just char

parseTapeToString :: [Maybe Char] -> String
parseTapeToString tape = map (\x -> getCharFromMaybeChar x) tape

getCharFromMaybeChar :: Maybe Char -> Char
getCharFromMaybeChar (Just x) = x
getCharFromMaybeChar Nothing = emptyChar

getSnapColor :: Bool -> Bool -> String
getSnapColor False False = ""
getSnapColor True False = "qA"
getSnapColor False True = "qR"

removeSelected :: String -> String
removeSelected str = intercalate " " (filter (\x -> x /= "selected") (splitOn " " str))

createSquareFor :: Char -> Int -> Int -> UI Element
createSquareFor char i index = do
    square <- UI.div # set text [char]
    let emptyCh = if (char == emptyChar) then " empty-char-square" else ""
    let indexedSquare = if (i == index) then " indexed-square" else ""
    element square # set (attr "class") ("square" ++ indexedSquare ++ emptyCh)

createSpanFor :: Snap -> Window -> String -> String -> UI Element
createSpanFor snap window qa qr = do
    spanElement <- UI.span # set text (actualState snap)
    element spanElement # set (attr "id") (idSnap snap)
    let color = getSnapColor (actualState snap == qa) (actualState snap == qr) 
    element spanElement # set (attr "class") ("label snap "++color)
    element spanElement # set (attr "data-info") (parseSnapToData snap)
    on UI.click spanElement $ \_ -> do
        selectedSnap <- getElementById window "selected-snap"
        doCreateSpanFor selectedSnap spanElement color window snap
    element spanElement

doCreateSpanFor :: Maybe Element -> Element -> String -> Window -> Snap -> UI Element
doCreateSpanFor (Just divSelectedSnap) spanElement color window snap = do
    let charsInTape = zip [0..] (parseTapeToString (tape snap)) :: [(Int, Char)]
    let index = tapeLocation snap
    squares <- mapM (\(i,x) -> createSquareFor x i index) charsInTape
    snapsElements <- getElementsByClassName window "snap"
    classes <- mapM (getAttr "class") snapsElements
    let snapsElementsWithIndex = zip [0..] snapsElements :: [(Int, Element)]
    mapM (\(i,x) -> element x #. (removeSelected (classes !! i))) snapsElementsWithIndex
    element spanElement #. ("label snap selected "++color)
    element divSelectedSnap # set children squares
doCreateSpanFor Nothing spanElement color window snap= error "No div selected snap found."


createSpanWithEntryFor :: Snap -> Window -> String -> String -> UI Element
createSpanWithEntryFor snap window qa qr = do
    newEntry <- UI.div
    element newEntry # set (attr "id") ("entry-"++idSnap snap)
    element newEntry # set (attr "class") "entry"
    element newEntry #+ [createSpanFor snap window qa qr]

parseSnapToData :: Snap -> String
parseSnapToData snap = "{\"branch\": \""++(parent snap)++
    "\", \"id\": \""++(idSnap snap)++
    "\", \"tape\": \""++(parseTapeToString (tape snap))++
    "\", \"index\": \""++(show (tapeLocation snap))++
    "\", \"depth\": \""++(show (depth snap))++
    "\", \"state\": \""++(actualState snap)++ "\"}"

getAsPair :: [String] -> (String, String)
getAsPair (x:xs) = (x, head xs)

getValue :: (String, String) -> String
getValue (k,v) = v

getAttributeFromList :: [(String, String)] -> String -> String
getAttributeFromList attributes key = getValue (head info) where info = filter (\(k,v) -> k == key) attributes

makeSnap :: [(String, String)] -> Snap
makeSnap attributes = Snap { idSnap = getAttributeFromList attributes "id",
    parent = getAttributeFromList attributes "branch",
    depth = read (getAttributeFromList attributes "depth") :: Int,
    tape = parseStringToTape (getAttributeFromList attributes "tape"),
    actualState = getAttributeFromList attributes "state",
    tapeLocation = read (getAttributeFromList attributes "index") :: Int }

infoWithoutSpecialChars :: String -> String
infoWithoutSpecialChars info = filter (\x -> not (elem x ['{','}','"'])) info

parseDataToSnap :: String -> Snap
parseDataToSnap info = makeSnap attributes 
    where attributes = map (\x -> getAsPair (splitOn ": " x)) (splitOn ", " (infoWithoutSpecialChars info))

parseListToSnaps :: [String] -> [Snap]
parseListToSnaps ls = map parseDataToSnap ls

initialSnap :: String -> String -> Snap
initialSnap w q0 = Snap { idSnap = "d0s0t0", 
    parent = "wrapper-tree", 
    depth = 0, 
    tape = parseStringToTape w, 
    actualState = q0, 
    tapeLocation = 0 }

qaDepth :: [Snap] -> String
qaDepth xs = show (minimum (map (\x -> depth x) xs) + 1)

createSnapList :: [String] -> Int -> TuringMachine -> String -> [Snap] 
createSnapList snapInfoList 0 tmDefinition w = [initialSnap w (q0 tmDefinition)]
createSnapList snapInfoList length tmDefinition w = parseListToSnaps snapInfoList

getMaxDepthOf :: [Snap] -> Int
getMaxDepthOf [] = -1
getMaxDepthOf snapList = maximum (map (\x -> depth x) snapList)

filterSnapsInDepth :: [Snap] -> Int -> TuringMachine -> [Snap]
filterSnapsInDepth snapList maxDepth tmDefinition = filter (\x -> depth x == maxDepth && actualState x /= (qr tmDefinition) && actualState x /= (qa tmDefinition)) snapList

filterSnapsWithQA :: TuringMachine -> [Snap] -> [Snap]
filterSnapsWithQA tmDefinition snapList = filter (\x -> actualState x == (qa tmDefinition)) snapList

doSetMsgFor :: String -> String -> Maybe Element -> Maybe Element -> Maybe Element -> UI ()
doSetMsgFor msg color (Just execData) (Just msgErrDiv) (Just execTree) = do
    element execData #. "row upper-container divider-secondary border-white"
    element execTree #. "middle-container divider-secondary with-msg-error"
    span <- UI.span # set text msg
    element msgErrDiv #. ("msg-container divider-secondary border-white "++ color)
    element msgErrDiv # set children [span]
    return ()
doSetMsgFor msg color _ _ _ = error "Can't set msg"

setMsgFor :: Window -> String -> String -> UI ()
setMsgFor window color msg = do
    executionData <- getElementById window "execution-data"
    msgErrorDiv <- getElementById window "msg-error"
    executionTree <- getElementById window "execution-tree"
    doSetMsgFor msg color executionData msgErrorDiv executionTree

doRemoveError :: Maybe Element -> Maybe Element -> Maybe Element -> UI ()
doRemoveError (Just execData) (Just msgErrDiv) (Just execTree) = do
    element execData #. "row upper-container divider-secondary"
    element execTree #. "middle-container divider-secondary"
    span <- UI.span # set text ""
    element msgErrDiv #. "msg-container divider-secondary border-white hidden"
    element msgErrDiv # set children [span]
    return ()
doRemoveError _ _ _ = error "Can't remove msg"

removeError :: Window -> UI ()
removeError window = do
    executionData <- getElementById window "execution-data"
    msgErrorDiv <- getElementById window "msg-error"
    executionTree <- getElementById window "execution-tree"
    doRemoveError executionData msgErrorDiv executionTree

getaz :: Bool -> [Char]
getaz True = ['a'..'z']
getaz False = []

getAZ :: Bool -> [Char]
getAZ True = ['A'..'Z']
getAZ False = []

get09 :: Bool -> [Char]
get09 True = ['0'..'9']
get09 False = []

parseStates :: String -> [String]
parseStates states = deleteDuplicatedElements (splitOn "," (filter (\x -> not (elem x [' '])) states))

deleteWhitespacesAndComma :: String -> String
deleteWhitespacesAndComma str = filter (\x -> not (elem x [',',' '])) str

deleteDuplicatedElements :: [String] -> [String]
deleteDuplicatedElements = map head . group . sort

validateStates :: [String] -> String -> String -> String -> Bool
validateStates states q0 qA qR = (states /= [""]) && (q0 /= "") && (qA /= "") && (qR /= "") && (qA /= qR) && (elem q0 states) && (elem qA states) && (elem qR states)

validateW :: String -> [Char] -> Bool
validateW w totalSigma = foldl (&&) True (map (\x -> elem x totalSigma) w)

validateSigma :: [Char] -> Bool
validateSigma totalSigma = not (elem emptyChar totalSigma)

validateGamma :: [Char] -> [Char] -> Bool
validateGamma totalGamma totalSigma = foldl (&&) True (map (\x -> elem x totalGamma) totalSigma)

validateTriplet :: Triplet -> [String] -> String -> Bool
validateTriplet triplet states gamma = (elem (setState triplet) states) &&
    (elem (getCharFromMaybeChar (setChar triplet)) gamma) &&
    (elem (show (movement triplet)) ["L","R","S"])

validateTransition :: Transition -> [String] -> String -> Bool
validateTransition transition states gamma = (elem (withState transition) states) &&
    (elem (getCharFromMaybeChar (withChar transition)) gamma) &&
    (foldl (&&) True (map (\x -> validateTriplet x states gamma) (triplets transition)))

validateDelta :: Maybe [Transition] -> [String] -> String -> Bool
validateDelta Nothing states gamma = False
validateDelta (Just delta) states gamma = foldl (&&) True (map (\x -> validateTransition x states gamma) delta)

setClickEvent :: Window -> String -> (Window -> UI ()) -> UI ()
setClickEvent window id event = do 
    button <- getElementById window id
    doSetClickEvent window button event

doSetClickEvent window (Just btn) event = on UI.click btn $ \_ -> event window
doSetClickEvent window Nothing event  = error "No run button found."

getAttr attr el = callFunction $ ffi "$(%1).attr(%2)" el attr
getLocalStorageItem key = callFunction $ ffi "window.localStorage.getItem(%1) || ''" key
setLocalStorageItem key value = runFunction $ ffi "window.localStorage.setItem(%1, %2)" key value

createOptionFor :: String -> UI Element
createOptionFor stateDesc = do
    option <- UI.option # set value stateDesc
    element option # set text stateDesc

setSelect :: Element -> [String] -> UI Element
setSelect select states = do
    options <- mapM (\x -> createOptionFor x) states  
    defaultOption <- UI.option # set value ""
    element defaultOption # set text "Select state"
    let totalOptions = [defaultOption]++options
    prevValue <- get value select
    element select # set children totalOptions
    let newValue = if (elem prevValue states) then prevValue else ""
    element select # set value newValue

setStatesChangeEvent :: Window -> UI ()
setStatesChangeEvent window = do 
    statesInput <- getElementById window "states"
    doSetStatesChangeEvent window statesInput
        
doSetStatesChangeEvent :: Window -> Maybe Element -> UI ()
doSetStatesChangeEvent window (Just stInput) = on UI.blur stInput $ \_ -> do
    val <- get value stInput
    let states = parseStates val
    selects <- getElementsByClassName window "select-state"
    mapM (\x -> setSelect x states) selects
doSetStatesChangeEvent window Nothing = error "No states input found."

updateSelects :: Window -> UI ()
updateSelects window = do
    selectQ0 <- getElementById window "select-q0"
    selectQA <- getElementById window "select-qA"
    selectQR <- getElementById window "select-qR"
    doUpdateSelects window selectQ0 selectQA selectQR

doUpdateSelects :: Window -> Maybe Element -> Maybe Element -> Maybe Element -> UI ()
doUpdateSelects window (Just selQ0) (Just selQA) (Just selQR) = do
    selQ0Value <- (getLocalStorageItem "select-q0")
    selQAValue <- (getLocalStorageItem "select-qA")
    selQRValue <- (getLocalStorageItem "select-qR")
    element selQ0 # set value selQ0Value
    element selQA # set value selQAValue
    element selQR # set value selQRValue
    on UI.blur selQ0 $ \_ -> do
        value <- get value selQ0
        setLocalStorageItem "select-q0" value
    on UI.blur selQA $ \_ -> do
        value <- get value selQA
        setLocalStorageItem "select-qA" value
    on UI.blur selQR $ \_ -> do
        value <- get value selQR
        setLocalStorageItem "select-qR" value
doUpdateSelects window _ _ _ = error "HTML selects not found."

strAsBool :: String -> Bool
strAsBool "True" = True
strAsBool _ = False

getValuesFromLocalStorageAndSetEvents :: Window -> UI ()
getValuesFromLocalStorageAndSetEvents window = do
    inputStates <- getElementById window "states"
    inputSigma <- getElementById window "sigma"
    checkSigmaaz <- getElementById window "sigma-az"
    checkSigmaAZ <- getElementById window "sigma-AZ"
    checkSigma09 <- getElementById window "sigma-09"
    inputGamma <- getElementById window "gamma"
    checkGammaaz <- getElementById window "gamma-az"
    checkGammaAZ <- getElementById window "gamma-AZ"
    checkGamma09 <- getElementById window "gamma-09"
    inputDelta <- getElementById window "delta"
    inputW <- getElementById window "w"
    doGetValuesFromLocalStorageAndSetEvents window inputStates (inputSigma, checkSigmaaz, checkSigmaAZ, checkSigma09) (inputGamma, checkGammaaz, checkGammaAZ, checkGamma09) (inputDelta, inputW)
    
doGetValuesFromLocalStorageAndSetEvents:: Window -> Maybe Element ->
    (Maybe Element, Maybe Element, Maybe Element, Maybe Element) ->
    (Maybe Element, Maybe Element, Maybe Element, Maybe Element) ->
    (Maybe Element, Maybe Element) -> UI ()
doGetValuesFromLocalStorageAndSetEvents window (Just inpStates)
    (Just inpSig, Just checkSaz, Just checkSAZ, Just checkS09)
    (Just inpGam, Just checkGaz, Just checkGAZ, Just checkG09) 
    (Just inpDel, Just inpW) = do
        inpStatesValue <- (getLocalStorageItem "states")
        inpSigValue <- (getLocalStorageItem "sigma")
        checkSazValue <- (getLocalStorageItem "sigma-az")
        checkSAZValue <- (getLocalStorageItem "sigma-AZ")
        checkS09Value <- (getLocalStorageItem "sigma-09")
        inpGamValue <- (getLocalStorageItem "gamma")
        checkGazValue <- (getLocalStorageItem "gamma-az")
        checkGAZValue <- (getLocalStorageItem "gamma-AZ")
        checkG09Value <- (getLocalStorageItem "gamma-09")
        inpDelValue <- (getLocalStorageItem "delta")
        inpWValue <- (getLocalStorageItem "w")
        element inpStates # set value inpStatesValue
        element inpSig # set value inpSigValue
        element checkSaz # set UI.checked (strAsBool checkSazValue)
        element checkSAZ # set UI.checked (strAsBool checkSAZValue)
        element checkS09 # set UI.checked (strAsBool checkS09Value)
        element inpGam # set value inpGamValue
        element checkGaz # set UI.checked (strAsBool checkGazValue)
        element checkGAZ # set UI.checked (strAsBool checkGAZValue)
        element checkG09 # set UI.checked (strAsBool checkG09Value)
        element inpDel # set value inpDelValue
        element inpW # set value inpWValue

        let states = parseStates inpStatesValue
        selects <- getElementsByClassName window "select-state"
        mapM (\x -> setSelect x states) selects

        on UI.blur inpStates $ \_ -> do
            value <- get value inpStates
            setLocalStorageItem "states" value
        on UI.blur inpSig $ \_ -> do
            value <- get value inpSig
            setLocalStorageItem "sigma" value
        on UI.blur checkSaz $ \_ -> do
            value <- get UI.checked checkSaz
            setLocalStorageItem "sigma-az" (show value)
        on UI.blur checkSAZ $ \_ -> do
            value <- get UI.checked checkSAZ
            setLocalStorageItem "sigma-AZ" (show value)
        on UI.blur checkS09 $ \_ -> do
            value <- get UI.checked checkS09
            setLocalStorageItem "sigma-09" (show value)
        on UI.blur inpGam $ \_ -> do
            value <- get value inpGam
            setLocalStorageItem "gamma" value
        on UI.blur checkGaz $ \_ -> do
            value <- get UI.checked checkGaz
            setLocalStorageItem "gamma-az" (show value)
        on UI.blur checkGAZ $ \_ -> do
            value <- get UI.checked checkGAZ
            setLocalStorageItem "gamma-AZ" (show value)
        on UI.blur checkG09 $ \_ -> do
            value <- get UI.checked checkG09
            setLocalStorageItem "gamma-09" (show value)
        on UI.blur inpDel $ \_ -> do
            value <- get value inpDel
            setLocalStorageItem "delta" value
        on UI.blur inpW $ \_ -> do
            value <- get value inpW
            setLocalStorageItem "w" value
doGetValuesFromLocalStorageAndSetEvents window _ (_, _, _, _) (_, _, _, _) (_, _) = error "HTML element not found."