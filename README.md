Ejemplos de NTMs
Maquina M reconocedora tal que L(M) = L1 U L2
Siendo:
L1 = {0n1| n >= 0} → por ejemplo: si w = 0001, entonces w ∈ L1.
L2 = {1n0| n >= 0} → por ejemplo: si w = 0, entonces w ∈ L2.

Estados:
qInit, q0L1, q0L2, q1L1, q1L2, qA, qR

q0= qInit
qA= qA
qR= qR
Sigma= 0,1
Delta= 0,1

Delta:

(qInit, 0) = {(q0L1, 0, R);(q0L2, 0, R)}
(qInit, 1) = {(q1L1, 1, R);(q1L2, 1, R)}
(q0L1, 0) = (q0L1, 0, R)
(q0L1, 1) = (q1L1, 1, R)
(q1L1, ß) = (qA, ß, S)
(q1L2, 0) = (q0L2, 0, R)
(q1L2, 1) = (q1L2, 1, R)
(q0L2, ß) = (qA, ß, S)


Maquina generadora de todas las cadenas de 0 y 1 de longitud 4:

Estados:
q0,q1,q2,q3,qa,qr

q0= q0
qA= qa
qR= qr
Sigma= 0,1
Delta= 0,1

Delta:

(q0,ß)={(q1,1,R);(q1,0,R)}
(q1,ß)={(q2,1,R);(q2,0,R)}
(q2,ß)={(q3,1,R);(q3,0,R)}
(q3,ß)={(qa,1,R);(qa,0,R)}