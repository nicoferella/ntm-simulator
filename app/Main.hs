module Main where

import Control.Monad
import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny.Core
import Data.ByteString.Char8 (pack)
import TM
import Helper

main :: IO ()
main = startGUI defaultConfig 
    { jsCustomHTML     = Just "index.html"
    , jsStatic         = Just "static"
    , jsPort           = Just 8023
    , jsAddr           = Just (pack "localhost")
    } setup

setup :: Window -> UI ()
setup window = do
    setClickEvent window "run" (run "runAll")
    setClickEvent window "run-step" (run "runStep")
    setClickEvent window "run-qA" (run "untilQa")
    setClickEvent window "reset" reset
    setStatesChangeEvent window
    getValuesFromLocalStorageAndSetEvents window
    updateSelects window

run :: String -> Window -> UI ()
run mode window = do
    inputStates <- getElementById window "states"
    selectQ0 <- getElementById window "select-q0"
    selectQA <- getElementById window "select-qA"
    selectQR <- getElementById window "select-qR"
    inputSigma <- getElementById window "sigma"
    checkSigmaaz <- getElementById window "sigma-az"
    checkSigmaAZ <- getElementById window "sigma-AZ"
    checkSigma09 <- getElementById window "sigma-09"
    inputGamma <- getElementById window "gamma"
    checkGammaaz <- getElementById window "gamma-az"
    checkGammaAZ <- getElementById window "gamma-AZ"
    checkGamma09 <- getElementById window "gamma-09"
    inputDelta <- getElementById window "delta"
    inputW <- getElementById window "w"
    doRun mode window
          (inputStates, selectQ0, selectQA, selectQR) 
          (inputSigma, checkSigmaaz, checkSigmaAZ, checkSigma09)
          (inputGamma, checkGammaaz, checkGammaAZ, checkGamma09) 
          (inputDelta, inputW)
    
doRun :: String -> Window ->
    (Maybe Element, Maybe Element, Maybe Element, Maybe Element) ->
    (Maybe Element, Maybe Element, Maybe Element, Maybe Element) ->
    (Maybe Element, Maybe Element, Maybe Element, Maybe Element) ->
    (Maybe Element, Maybe Element) -> UI ()
doRun mode window
      (Just inpStates, Just selQ0, Just selQA, Just selQR) 
      (Just inpSig, Just checkSaz, Just checkSAZ, Just checkS09)
      (Just inpGam, Just checkGaz, Just checkGAZ, Just checkG09) 
      (Just inpDel, Just inpW) = do
    states <- get value inpStates
    q0 <- get value selQ0
    qA <- get value selQA
    qR <- get value selQR
    sigma <- get value inpSig
    sigmaaz <- get UI.checked checkSaz
    sigmaAZ <- get UI.checked checkSAZ
    sigma09 <- get UI.checked checkS09
    gamma <- get value inpGam
    gammaaz <- get UI.checked checkGaz
    gammaAZ <- get UI.checked checkGAZ
    gamma09 <- get UI.checked checkG09
    deltaStr <- get value inpDel
    w <- get value inpW
    let totalSigma = (deleteWhitespacesAndComma sigma) ++ getaz sigmaaz ++ getAZ sigmaAZ ++ get09 sigma09
    let totalGamma = (deleteWhitespacesAndComma gamma) ++ getaz gammaaz ++ getAZ gammaAZ ++ get09 gamma09 ++ [emptyChar]
    let parsedDelta = deltaParser deltaStr
    let parsedStates = parseStates states
    let statesValidation = validateStates parsedStates q0 qA qR
    let wValidation = validateW w totalSigma
    let sigmaValidation = validateSigma totalSigma   
    let gammaValidation = validateGamma totalGamma totalSigma
    let deltaValidation = validateDelta parsedDelta parsedStates totalGamma
    doRunWith window mode w 
        (parsedStates, totalSigma, totalGamma, q0, qA, qR) 
        (statesValidation, wValidation, sigmaValidation, gammaValidation, deltaValidation, parsedDelta)        
doRun mode window _ _ _ _ = error "HTML element not found."

doRunWith :: Window -> String -> String -> ([String], [Char], [Char], String, String, String) -> (Bool, Bool, Bool, Bool, Bool, Maybe [Transition]) -> UI ()
doRunWith window mode w (parsedStates, totalSigma, totalGamma, q0, qA, qR) (False, _, _, _, _, _) = setMsgFor window "red" "Error: debe definir estados. q0, qA y qR deben ser estados posibles. qA y qR deben ser distintos"
doRunWith window mode w (parsedStates, totalSigma, totalGamma, q0, qA, qR) (_, False, _, _, _, _) = setMsgFor window "red" "Error: w debe contener solamente caracteres definidos en sigma"
doRunWith window mode w (parsedStates, totalSigma, totalGamma, q0, qA, qR) (_, _, False, _, _, _) = setMsgFor window "red" "Error: sigma no debe contener el caracter que representa el vacío (ß)"
doRunWith window mode w (parsedStates, totalSigma, totalGamma, q0, qA, qR) (_, _, _, False, _, _) = setMsgFor window "red" "Error: todos los caracteres de sigma deben estar incluidos en gamma"
doRunWith window mode w (parsedStates, totalSigma, totalGamma, q0, qA, qR) (_, _, _, _, False, Nothing) = setMsgFor window "red" "Error: la sintaxis de delta no es correcta"
doRunWith window mode w (parsedStates, totalSigma, totalGamma, q0, qA, qR) (_, _, _, _, False, Just deltaP) = setMsgFor window "red" "Error: el delta debe tener estados válidos, debe leer/escribir solamente caracteres definidos en gamma y debe tener movimientos válidos"
doRunWith window mode w (parsedStates, totalSigma, totalGamma, q0, qA, qR) (True, True, True, True, True, Just deltaP) = do
    let tmDefinition = makeTmDefinition parsedStates totalSigma totalGamma deltaP q0 qA qR
    removeError window
    updateTree window tmDefinition w mode

reset :: Window -> UI ()
reset window = do
    wrapper <- getElementById window "wrapper-tree"
    selectedSnap <- getElementById window "selected-snap"
    doReset window wrapper selectedSnap

doReset :: Window -> Maybe Element -> Maybe Element -> UI ()
doReset window (Just divWrapper) (Just selSnap) = do
    element divWrapper # set children []
    element selSnap # set children []
    removeError window
doReset window _ _ = error "No div wrapper or selected snap found."

makeTmDefinition :: [String] -> [Char] -> [Char] -> [Transition] -> String -> String -> String -> TuringMachine
makeTmDefinition states sigma gamma delta stateQ0 stateQA stateQR = TuringMachine { 
    qs = states, 
    sigma = sigma, 
    gamma = gamma,
    delta = delta,
    q0 = stateQ0,
    qa = stateQA,
    qr = stateQR
}

updateTree :: Window -> TuringMachine -> String -> String -> UI ()
updateTree window tmDefinition w mode = do
    snapsElements <- getElementsByClassName window "snap"
    snapInfoList <- mapM (getAttr "data-info") snapsElements
    let snapList = createSnapList snapInfoList (length snapsElements) tmDefinition w
    let maxDepth = getMaxDepthOf snapList
    let snapsInDepth = filterSnapsInDepth snapList maxDepth tmDefinition 
    let qas = filterSnapsWithQA tmDefinition snapList
    let qaReached = qas /= []      
    executionBehaviour window tmDefinition w snapsInDepth mode qaReached qas (length snapsElements == 0) maxDepth

executionBehaviour :: Window -> TuringMachine -> String -> [Snap] -> String -> Bool -> [Snap] -> Bool -> Int -> UI ()
executionBehaviour window tmDefinition w snapsInDepth   "untilQa" True        qas initialize maxDepth = setMsgFor window "green" ("Estado qa alcanzado en "++(qaDepth qas)++" paso(s)")
executionBehaviour window tmDefinition w snapsInDepth   mode      qaReached   qas initialize maxDepth = do
    let executedSnaps = getExecutedSnaps initialize snapsInDepth tmDefinition
    addElementToTree window executedSnaps (qa tmDefinition) (qr tmDefinition)
    let hasNewSnapsToExecute = (length executedSnaps /= 0)
    checkContinueExecution window tmDefinition w mode qaReached qas snapsInDepth hasNewSnapsToExecute maxDepth

getExecutedSnaps :: Bool -> [Snap] -> TuringMachine -> [Snap]
getExecutedSnaps True snapsInDepth tmDefinition = snapsInDepth
getExecutedSnaps False snapsInDepth tmDefinition = executeSnapsOneStep snapsInDepth (delta tmDefinition)

checkContinueExecution :: Window -> TuringMachine -> String -> String -> Bool -> [Snap] -> [Snap] -> Bool -> Int -> UI ()
-- si no hay mas para ejecutar
checkContinueExecution window tmDefinition w mode       True        qas snapsInDepth False maxDepth = setMsgFor window "green" ("Estado qa alcanzado en "++(qaDepth qas)++" paso(s). Ejecución finalizada en "++(show (maxDepth+1))++" paso(s)")
checkContinueExecution window tmDefinition w mode       False       qas snapsInDepth False maxDepth = setMsgFor window "red" ("No se ha alcanzado qa en ninguna de las ejecuciones. Ejecución finalizada en "++(show (maxDepth+1))++" paso(s)")
-- si puede continuar hay que chequear el mode y el qaReached
checkContinueExecution window tmDefinition w "runAll"   qaReached   qas snapsInDepth True maxDepth = updateTree window tmDefinition w "runAll"
checkContinueExecution window tmDefinition w "untilQa"  False       qas snapsInDepth True maxDepth = updateTree window tmDefinition w "untilQa"
checkContinueExecution window tmDefinition w "untilQa"  True        qas snapsInDepth True maxDepth = setMsgFor window "green" ("Estado qa alcanzado en "++(qaDepth qas)++" paso(s). Ejecución finalizada en "++(show (maxDepth+1))++" paso(s)")
checkContinueExecution window tmDefinition w "runStep"  True        qas snapsInDepth True maxDepth = setMsgFor window "green" ("Estado qa alcanzado en "++(qaDepth qas)++" paso(s)")
checkContinueExecution window tmDefinition w mode       qaReached   qas snapsInDepth True maxDepth = return ()

addElementToTree :: Window -> [Snap] -> String -> String -> UI ()
addElementToTree window [] qa qr = return ()
addElementToTree window (x:xs) qa qr = doAddElementToTree window (x:xs) qa qr (parent x)

doAddElementToTree :: Window -> [Snap] -> String -> String -> String -> UI ()
doAddElementToTree window (x:xs) qa qr "wrapper-tree" = do
    wrapper <- getElementById window (parent x)
    doAddFirstCase window (x:xs) qa qr wrapper 
doAddElementToTree window (x:xs) qa qr _ = do
    parentSpan <- getElementById window (parent x)
    branch <- getElementById window ("branch-"++parent x)
    doAddNextCases window (x:xs) qa qr parentSpan branch
        
doAddFirstCase :: Window -> [Snap] -> String -> String -> Maybe Element -> UI ()
doAddFirstCase window (x:xs) qa qr (Just divWrapper) = do
    element divWrapper #+ [createSpanFor x window qa qr]
    addElementToTree window xs qa qr
doAddFirstCase window (x:xs) qa qr Nothing = error ("No div wrapper found."++" for "++(parent x))

doAddNextCases :: Window -> [Snap] -> String -> String -> Maybe Element -> Maybe Element -> UI ()
doAddNextCases window (x:xs) qa qr (Just span) (Just divBranch) = do
    element divBranch #+ [createSpanWithEntryFor x window qa qr]
    addElementToTree window xs qa qr
doAddNextCases window (x:xs) qa qr (Just span) Nothing = do
    newBranch <- UI.div
    element newBranch # set (attr "id") ("branch-"++parent x)
    element newBranch # set (attr "class") "branch"
    element newBranch #+ [createSpanWithEntryFor x window qa qr]
    let idDivParent = if (parent x == "d0s0t0") then "wrapper-tree" else ("entry-"++parent x)
    parentDiv <- getElementById window idDivParent
    doAddBranch window (x:xs) qa qr newBranch parentDiv
doAddNextCases window (x:xs) qa qr Nothing Nothing = error ("No span found."++" for "++(parent x))

doAddBranch :: Window -> [Snap] -> String -> String -> Element -> Maybe Element -> UI ()
doAddBranch window (x:xs) qa qr newBranch (Just pDiv) = do
    element pDiv #+ [element newBranch]
    addElementToTree window xs qa qr
doAddBranch window (x:xs) qa qr newBranch Nothing = error ("No div entry found."++" for "++(parent x))